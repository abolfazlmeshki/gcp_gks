variable "gke_zone" {
  type        = string
  default     = "europe-west3-a"
  description = "ZOne of the Cluster"
}

variable "gke_name" {
  type        = string
  default     = "meshki"
  description = "Name of the GKE CLuster"
}

variable "gke_master_authorized_network_cidrs" {
  type        = list(any)
  description = "A list of up to 20 maps containing `master_authorized_network_cidrs` and `display_name` keys, representing source network CIDRs that are allowed to connect master nodes over HTTPS."

  default = [
    {
      ## this needs to be changed when enable_private_endpoint is true
      cidr_block   = "0.0.0.0/0"
      display_name = "local range allowed"
    },
  ]
}

variable "gke_vpc_network" {
  type        = string
  default     = "management"
  description = "which VPC to choose"
}

variable "gke_vpc_subnet" {
  type        = string
  default     = "management"
  description = "which VPC to choose"
}

variable "gke_vpa_enabled" {
  type        = string
  default     = false
  description = "A boolean to enable VPA for the cluster"

}

variable "gke_monitoring_service" {
  type        = string
  description = "The monitoring service to write metrics to"
  default     = "monitoring.googleapis.com/kubernetes"
}

variable "gke_logging_service" {
  type        = string
  description = "The logging service to write logs to"
  default     = "logging.googleapis.com/kubernetes"
}






