output "cluster_name" {
  value       = resource.google_container_cluster.gke.name
  description = "GKE Cluster Name"
}

output "endpoint" {
  value       = resource.google_container_cluster.gke.endpoint
  description = "GKE Cluster Host"
}


output "cluster_ca_certificate" {
  value       = resource.google_container_cluster.gke.master_auth[0].cluster_ca_certificate
  description = "CA Certificate of EKS Cluster"
}

