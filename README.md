<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | 4.46.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | 3.4.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | 4.48.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.4.3 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_container_cluster.gke](https://registry.terraform.io/providers/hashicorp/google/4.46.0/docs/resources/container_cluster) | resource |
| [google_container_node_pool.gke_nodes](https://registry.terraform.io/providers/hashicorp/google/4.46.0/docs/resources/container_node_pool) | resource |
| [google_service_account.gke_sa](https://registry.terraform.io/providers/hashicorp/google/4.46.0/docs/resources/service_account) | resource |
| [random_integer.random_id](https://registry.terraform.io/providers/hashicorp/random/3.4.3/docs/resources/integer) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gke_logging_service"></a> [gke\_logging\_service](#input\_gke\_logging\_service) | The logging service to write logs to | `string` | `"logging.googleapis.com/kubernetes"` | no |
| <a name="input_gke_master_authorized_network_cidrs"></a> [gke\_master\_authorized\_network\_cidrs](#input\_gke\_master\_authorized\_network\_cidrs) | A list of up to 20 maps containing `master_authorized_network_cidrs` and `display_name` keys, representing source network CIDRs that are allowed to connect master nodes over HTTPS. | `list(any)` | <pre>[<br>  {<br>    "cidr_block": "0.0.0.0/0",<br>    "display_name": "local range allowed"<br>  }<br>]</pre> | no |
| <a name="input_gke_monitoring_service"></a> [gke\_monitoring\_service](#input\_gke\_monitoring\_service) | The monitoring service to write metrics to | `string` | `"monitoring.googleapis.com/kubernetes"` | no |
| <a name="input_gke_name"></a> [gke\_name](#input\_gke\_name) | Name of the GKE CLuster | `string` | `"meshki"` | no |
| <a name="input_gke_vpa_enabled"></a> [gke\_vpa\_enabled](#input\_gke\_vpa\_enabled) | A boolean to enable VPA for the cluster | `string` | `false` | no |
| <a name="input_gke_vpc_network"></a> [gke\_vpc\_network](#input\_gke\_vpc\_network) | which VPC to choose | `string` | `"management"` | no |
| <a name="input_gke_vpc_subnet"></a> [gke\_vpc\_subnet](#input\_gke\_vpc\_subnet) | which VPC to choose | `string` | `"management"` | no |
| <a name="input_gke_zone"></a> [gke\_zone](#input\_gke\_zone) | ZOne of the Cluster | `string` | `"europe-west3-a"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#output\_cluster\_ca\_certificate) | CA Certificate of EKS Cluster |
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | GKE Cluster Name |
| <a name="output_endpoint"></a> [endpoint](#output\_endpoint) | GKE Cluster Host |
<!-- END_TF_DOCS -->