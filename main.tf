resource "random_integer" "random_id" {
  min = 1000
  max = 9999
}
resource "google_service_account" "gke_sa" {
  account_id   = "${var.gke_name}-${random_integer.random_id.result}"
  display_name = "${var.gke_name}_sa"
  description  = "Used for GKE CLuster named: ${var.gke_name}"
}

resource "google_container_cluster" "gke" {
  name                     = var.gke_name
  location                 = var.gke_zone
  remove_default_node_pool = true
  initial_node_count       = 1
  network                  = var.gke_vpc_network
  subnetwork               = var.gke_vpc_subnet
  monitoring_service       = var.gke_monitoring_service
  logging_service          = var.gke_logging_service
  #node_version          = "1.22.12-gke.2300" 
  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = "172.16.0.0/28"
  }
  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = var.gke_master_authorized_network_cidrs
      content {
        cidr_block   = cidr_blocks.value.cidr_block
        display_name = lookup(cidr_blocks.value, "display_name", null)
      }
    }
  }

  release_channel {
    channel = "UNSPECIFIED"
  }
  vertical_pod_autoscaling {
    enabled = var.gke_vpa_enabled
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = "pod"
    services_secondary_range_name = "service"
  }

  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }
  lifecycle {
    ignore_changes = [
      node_pool
    ]
  }
}

resource "google_container_node_pool" "gke_nodes" {
  name       = "${var.gke_name}-pool"
  location   = var.gke_zone
  cluster    = google_container_cluster.gke.name
  node_count = 1
  version    = "1.22.12-gke.2300"
  management {
    #auto_repair  = var.auto_repair
    auto_upgrade = false
  }
  node_config {
    preemptible  = true
    machine_type = "e2-standard-4"
    image_type   = "UBUNTU"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.gke_sa.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/compute",
    ]
  }

}
